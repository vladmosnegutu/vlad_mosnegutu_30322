package edu.utcn.lab12.vehicles;

import org.junit.Test;

public class ElectricBatteryTest {

    /**
     * Expect battery to throw exception if charged more than 100%
     * @throws BatteryException 
     */
    @Test(expected = BatteryException.class)
    public void charge() throws BatteryException{
        ElectricBattery bat = new ElectricBattery();
        for(int i=0;i<110;i++)
        	if(i>100) {
        		throw new BatteryException("The battery is full");
        	}
        	else
            bat.charge();

    }
}