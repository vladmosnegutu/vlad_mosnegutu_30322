package edu.utcn.lab12.devices;

public class TV extends ElectronicDevice{
    int channel;

    public TV(int channel) {
    	this.channel = channel;
    }

    public String channelUp(){
        if(channel<=10){
            channel++;
            System.out.println("Channel up "+channel);
        }
        return "Channel up "+channel;
    }

    public String channelDown(){
        if(channel>=2){
            channel--;
            System.out.println("Channel down "+channel);
        }
        return "Channel down "+channel;
    }

    public int getChannel() {
        return channel;
    }
}