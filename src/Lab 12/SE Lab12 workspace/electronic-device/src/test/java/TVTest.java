import org.junit.Test;

import static org.junit.Assert.*;

import edu.utcn.lab12.devices.TV;

public class TVTest {
	
	@Test
	public void testChannelUp() {
		TV tv = new TV(3);
		assertEquals("Channel up 4", tv.channelUp());
	}
	
	@Test
	public void testChannelDown() {
		TV tv = new TV(3);
		assertEquals("Channel down 2", tv.channelDown());
	}
}
