package Lab5;

public abstract class Sensor {
    private String location;

    public String getLocation() {
        return location;
    }

    public abstract int readValue();
}

