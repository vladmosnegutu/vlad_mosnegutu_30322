package Lab5;

public class ProgramControllerSingleton {
    public static void main(String[] args) throws InterruptedException {
        var controller = ControllerSingleton.create();
        controller.control();
    }
}
