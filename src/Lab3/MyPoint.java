package Lab3;

public class MyPoint {
    public double x;
    public double y;

    public MyPoint() {
        this.x = 0;
        this.y = 0;
    }

    public MyPoint(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double distance(double xp, double yp) {
        return Math.sqrt(Math.pow((xp - this.x), 2) + Math.pow((yp - this.y), 2));
    }

    public double distance(MyPoint p) {
        return Math.sqrt(Math.pow((p.x - this.x), 2) + Math.pow((p.y - this.y), 2));
    }
}
