package Lab3;

public class Flower {
    public int petal;
    public static int count = 0;

    public Flower(){
        System.out.println("New flower has been created!");
        count++;
    }

    public Flower(int p){
        petal=p;
        System.out.println("New flower has been created!");
        count++;
    }


    public static int getCount(){
        return count;
    }

    public static void main(String[] args) {
        Flower f1 = new Flower(3);
        Flower f2 = new Flower();
        Flower f3 = new Flower(6);
        Flower f4 = new Flower();

        System.out.println(getCount());

    }
}
