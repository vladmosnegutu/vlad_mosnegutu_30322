package Lab3;

public class TestMyPoint {

    public static void main(String [] args) {
        MyPoint a = new MyPoint(2.3, 4.4);
        MyPoint b = new MyPoint(5, 8);

        System.out.println(a.x+"\t"+ a.y);

        System.out.println("\n" + a.distance(3.0,4.0));
        System.out.println("\n"+ a.distance(b));
    }
}