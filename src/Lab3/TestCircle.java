package Lab3;
import java.util.Scanner;

public class TestCircle {

    public static void main(String [] args){
        double rad = 1.0; String col = "red";
        int sel = 0; Circle c;
        Scanner console = new Scanner(System.in);
        System.out.println("Create circle. For custom creation select (1)");
        sel = console.nextInt();
        switch(sel) {
            case 1:
                System.out.println("radius");
                rad = console.nextDouble();
                System.out.println("colour:");
                col = console.next();
                c = new Circle(rad, col);
                break;
            default:
                c = new Circle();
                break;
        }
        System.out.println("\n"+ c.getColour()+"\n"+ c.getRadius() );
    }
}
