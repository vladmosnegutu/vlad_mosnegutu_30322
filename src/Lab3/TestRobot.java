package Lab3;
import java.util.Scanner;

public class TestRobot {

    public static void main(String[] args){
        Robot r = new Robot();
        int k = 0;
        Scanner console = new Scanner(System.in);
        while(k>-1) {
            k = 0;
            k = console.nextInt();
            r.change(k);
            System.out.println(r); //printing the object will automatically call toString
        }
        System.out.println("Done testing");
    }
}