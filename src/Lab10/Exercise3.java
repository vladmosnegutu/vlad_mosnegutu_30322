package Lab10;

public class Exercise3  extends Thread {
    static int counter = 0;
    Thread t;
    Exercise3(String name, Thread t){
        super(name);
        this.t=t;
    }

    public void run(){
        System.out.println("Firul "+this.getName()+" a intrat in metoda run()");
        try
        {
            if (t!=null) t.join();
            for(int i=0;i<100;i++){
                counter++;
            System.out.println(getName() + " counter = "+counter);
            try {
                Thread.sleep((int)(Math.random() * 100));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println(getName() + " job finalised.");
        }
        catch(Exception e){e.printStackTrace();}

    }

    public static void main(String[] args) {
        Exercise3 c1 = new Exercise3("counter1",null);
        Exercise3 c2 = new Exercise3("counter2", c1);

        c1.start();
        c2.start();
    }
}

