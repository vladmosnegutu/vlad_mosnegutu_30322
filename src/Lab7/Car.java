package Lab7;

import java.io.*;

public class Car implements Serializable{
    private String model;
    private double price;

    public Car(){}

    public Car(String model, double price) {
        this.model = model;
        this.price = price;
    }

    Car createCar(String model, double price)
    {
        Car c = new Car(model, price);
        System.out.println("Car successfully created!");
        return c;
    }

    void saveCar(String fileName)
    {
        try{
            ObjectOutputStream o = new ObjectOutputStream(new FileOutputStream(fileName));
            o.writeObject(this);
            System.out.println("Car saved in a file!");
        }catch(IOException e){
            System.out.println("The car cannot be saved in a file!");
            e.printStackTrace();
        }
    }

    static Car loadCar(String fileName) throws IOException, ClassNotFoundException {
        ObjectInputStream in = new ObjectInputStream(new FileInputStream(fileName));
        Car c = (Car) in.readObject();
        return c;
    }

    static void readCar(String fileName)
    {
        try{
            Car c = Car.loadCar(fileName);
            System.out.println("Car model: " + c.model);
            System.out.println("Car price: " + c.price);
        }catch(IOException e){
            System.out.println("The car cannot be loaded from file!");
            e.printStackTrace();
        }catch(ClassNotFoundException e){
            System.out.println("The car class cannot be found!");
            e.printStackTrace();
        }
    }

    @Override
    public String toString() {
        return "Car{" +
                "model='" + model + '\'' +
                ", price=" + price +
                '}';
    }
}
