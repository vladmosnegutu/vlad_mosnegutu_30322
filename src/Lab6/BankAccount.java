package Lab6;

public class BankAccount {

    private String owner;
    public double balance;

    public BankAccount(double balance, String owner) {
        this.balance = balance;
        this.owner = owner;
    }

    public void withdraw(double amount){
        balance-=amount;
    }

    public void deposit(double amount){
        balance += amount;
    }

    public double getBalance() {
        return balance;
    }

    public String getOwner(){
        return owner;
    }
}
