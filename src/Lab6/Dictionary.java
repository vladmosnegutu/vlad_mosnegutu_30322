package Lab6;

import java.util.Collection;
import java.util.HashMap;
import java.util.Collections;
import java.util.Set;

public class Dictionary {

    HashMap<Word,Definition> hm = new HashMap<>();

    public void addWord(Word word, Definition def){
        this.hm.put(word,def);
    }

    public Definition getDefinition(Word word){
        return hm.get(word);
    }

    public Set<Word> getAllWords(){
        return hm.keySet();
    }

    public Collection<Definition> getDefinitions(){
        return hm.values();
    }


}
