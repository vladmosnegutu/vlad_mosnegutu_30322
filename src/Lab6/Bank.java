package Lab6;
import java.util.*;

public class Bank {

  public ArrayList<BankAccount> accounts = new ArrayList<>();

  public void addAccount(BankAccount account){
        accounts.add(account);
  }


    public void printAccounts() {
        sortThem(accounts);
        System.out.println("Sorted List by balance: ");

        for (int i = 0; i < accounts.size(); i++) {
            System.out.println(accounts.get(i).balance+ "\t" + accounts.get(i).getOwner());
        }
    }

    public void sortThem(ArrayList<BankAccount> acc){
        for(int i = 0; i <= acc.size(); i++)
            for(int j = 0; j < acc.size()-i-1; j++)
                if(acc.get(j).balance > acc.get(j+1).balance)
                {  double aux = acc.get(j).balance;
                    acc.get(j).balance = acc.get(j+1).balance;
                    acc.get(j+1).balance = aux;}
    }


    public void printAccounts(double min, double max){

      for(int i = 0; i< accounts.size(); i++){
          if((accounts.get(i).balance > min) && (accounts.get(i).balance < max))
              System.out.println(accounts.get(i).balance+ "\t" + accounts.get(i).getOwner());
      }

    }

    public ArrayList<BankAccount> getAccounts(){

            Collections.sort(accounts, new Comparator<BankAccount>() {

                public int compare(BankAccount a1, BankAccount a2) {
                    return a1.getOwner().compareToIgnoreCase(a2.getOwner());
                }
            });
                return accounts;
    }

  }
