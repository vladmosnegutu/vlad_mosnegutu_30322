package Lab4;

public class Rectangle extends Shape{
    private double width;
    private double length;
    public Rectangle()
    {
        super();
        this.width = 1;
        this.length = 1;
    }
    public Rectangle(double width, double length){
        super();
        this.width = width;
        this.length = length;
    }
    public Rectangle(String color, boolean filled, double width, double length){
        super(color,filled);
        this.width = width;
        this.length = length;
    }
    double getWidth()
    {
        return width;
    }
    double getLength(){
        return length;
    }
    void setWidth(double width){
        this.width = width;
    }
    void setLength(double length){
        this.length = length;
    }
    double getArea()
    {
        return length*width;
    }
    double getPerimeter()
    {
        return width + length;
    }
    @Override
    public String toString(){
        return "A rectangle with width=" + width +" and length=" + length + ", which is a subclass of "  + super.toString();
    }

}
