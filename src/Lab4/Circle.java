package Lab4;

public class Circle {

    private double radius ;
    private String color ;

    public Circle() {
        this.radius = 1.0;
        this.color = "red";
    }

    public Circle(double radius) {
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public double getArea() {
        return 3.14*radius*radius;
    }

}