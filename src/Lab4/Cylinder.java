package Lab4;

public class Cylinder extends Circle{
    double height;
    public Cylinder() {
        super();
        height = 1;
    }
    public Cylinder(double radius){
        super(radius);
        this.height = 1;
    }
    public Cylinder(double radius, double height){
        super(radius);
        this.height = height;
    }
    double getHeight(){
        return height;
    }
    double getVolume(){
        return super.getArea()*height;
    }
    @Override
    public double getArea(){
        return 2*3.14*getRadius()*super.getArea();
    }

}
