package Lab4;

public class Book {
    String name;
    Author author;
    double price;
    int qtyInStock;

    public Book(String name, Author author, double price)
    {
        this.name = name;
        this.author = author;
        this.price = price;
        this.qtyInStock = 0;
    }
    public Book(String name, Author author, double price, int qtyInStock)
    {
        this.name = name;
        this.author = author;
        this.price = price;
        this.qtyInStock = qtyInStock;
    }
    String getName() { return name;}

    Author getAuthor() { return author;}

    double getPrice() { return price;}

    void setPrice(double price) { this.price = price;}

    int getQtyInStock() { return qtyInStock;}

    void setQtyInStock(int qtyInStock) { this.qtyInStock = qtyInStock;}
    @Override
    public String toString() {
        return '\n' + name + " by " + author.toString();
    }
}
