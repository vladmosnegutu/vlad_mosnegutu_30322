package Lab4;

import Lab4.Circle;

import java.util.Scanner;

public class TestCircle {

    public static void main(String [] args){
        double rad = 1.0;
        int sel = 0; Lab4.Circle c;
        Scanner console = new Scanner(System.in);
        System.out.println("Create circle. For custom creation select (1)");
        sel = console.nextInt();
        switch(sel) {
            case 1:
                System.out.println("radius");
                rad = console.nextDouble();
                c = new Lab4.Circle(rad);
                break;
            default:
                c = new Circle();
                break;
        }
        System.out.println("\n area = "+ c.getArea()+"\n radius ="+ c.getRadius() );
    }
}
