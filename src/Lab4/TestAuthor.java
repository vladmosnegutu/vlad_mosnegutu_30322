package Lab4;

public class TestAuthor {

    public static void main(String [] args){
        Author autor = new Author("Ion Ionescu","ionionescu@lalamail.ro",'m');

        System.out.println(autor.getEmail()+"\t"+ autor.getName()+"\t"+autor.getGender());
        System.out.println("\n"+ autor.toString());

        autor.setEmail("ionionescu@gmail.com");

        System.out.println("\n"+ autor.toString());
    }
}