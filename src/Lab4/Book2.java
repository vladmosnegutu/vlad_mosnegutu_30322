package Lab4;

public class Book2 {
    String name;
    Author []authors;
    double price;
    int qtyInStock;

    public Book2(String name, Author []authors, double price)
    {
        this.name = name;
        this.authors = authors;
        this.price = price;
        this.qtyInStock = 0;
    }
    public Book2(String name, Author []authors, double price, int qtyInStock)
    {
        this.name = name;
        this.authors = authors;
        this.price = price;
        this.qtyInStock = qtyInStock;
    }
    String getName() { return name;}

    Author []getAuthor() { return authors;}

    double getPrice() { return price;}

    void setPrice(double price) { this.price = price;}

    int getQtyInStock() { return qtyInStock;}

    void setQtyInStock(int qtyInStock) { this.qtyInStock = qtyInStock;}
    @Override
    public String toString() {
        return '\n' + name + " by " + authors.length + " authors";
    }
    void printAuthors(){
        for(int i = 0; i< authors.length;i++) {
            System.out.println(authors[i]);
        }
    }
}

