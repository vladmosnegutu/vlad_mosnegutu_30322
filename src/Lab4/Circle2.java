package Lab4;

public class Circle2 extends Shape{
    private double radius ;

    public Circle2() {
        super();
        this.radius = 1.0;
    }

    public Circle2(double radius) {
        super();
        this.radius = radius;
    }
    public Circle2(String color, boolean filled, double radius) {
        super(color, filled);
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }
    void setRadius(double radius){
        this.radius = radius;
    }
    double getPerimeter(){
        return 2*3.14*radius;
    }
    public double getArea() {
        return 3.14*radius*radius;
    }

    @Override
    public String toString() {
        return "A Circle with radius=" +radius+ " which is a subclass of " + super.toString();
    }
}
