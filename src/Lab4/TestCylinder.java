package Lab4;

import java.util.Scanner;

public class TestCylinder {
    public static void main(String [] args){
        double rad = 1.0, hei = 1.0;
        int sel = 0; Cylinder c;
        Scanner console = new Scanner(System.in);
        System.out.println("Create cylinder. For custom creation select (1)");
        sel = console.nextInt();
        switch(sel) {
            case 1:
                System.out.println("radius");
                rad = console.nextDouble();
                System.out.println("height");
                hei = console.nextDouble();
                c = new Cylinder(rad, hei);
                break;
            default:
                c = new Cylinder();
                break;
        }
        System.out.println("\n area = "+ c.getArea()+"\n volume ="+ c.getVolume() );
    }
}
