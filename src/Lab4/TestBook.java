package Lab4;

public class TestBook {
    public static void main(String [] args){
        Author autor = new Author("Ion Ionescu","ionionescu@lalamail.ro",'m');

        System.out.println(autor.getEmail()+"\t"+ autor.getName()+"\t"+autor.getGender());
        System.out.println("\n"+ autor.toString());

        Book book = new Book("Motorina 10 lei", autor, 13.5);
        System.out.println("\n"+ book.getName());
        System.out.println("\n"+ book.getQtyInStock());
        System.out.println("\n"+ book.getPrice());
        Author test = book.getAuthor();
        System.out.println("\n"+ autor);
        System.out.println("\n"+ test);
        book.setPrice(9.45);
        System.out.println("\n"+ book.getPrice());
        book.setQtyInStock(22);
        System.out.println("\n"+ book.getQtyInStock());
        System.out.println("\n"+ book);


    }
}
