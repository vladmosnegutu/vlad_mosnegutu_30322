package Lab4;

public class Square extends Rectangle{
    public Square (){ super();}
    public Square(double side){
        super(side, side);
       // this.length = side;
       // this.width = side;
    }

    public Square(String color, boolean filled, double side) {
       super(color, filled, side, side);
       /* this.color = color;
        this.filled = filled;
        this.length = side;
        this. width = side; */
    }

    double getSide(){
        return super.getLength();
    }
    void setSide(double side){
        super.setLength (side); //because the attributes are not visible, only the set methods are
        super.setWidth (side);
    }
    @Override
    void setWidth(double side){
         super.setWidth (side);
    }
    @Override
    void setLength(double side){
        super.setWidth (side);
    }
    @Override
    public String toString(){
        return "A Square with side=" + super.getLength() + ", which is a subclass of "+super.toString();
    }

}
