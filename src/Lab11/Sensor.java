package Lab11;

import java.util.Observable;

public class Sensor extends Observable implements Runnable{
    static int MAX_VALUE=100;
    static int MIN_VALUE=0;
    double temp = 0;
    boolean active = true;
    boolean paused = false;
    Thread t;
    public void start(){
        if(t==null){
            t = new Thread(this);
            t.start();
        }
    }
    public void run(){
        while(active) {
            if (paused) {
                synchronized (this) {
                    try {
                        wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }


            tempRead();
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void setPause(boolean p){
        synchronized (this) {
            if(p==true){
                paused = true;
            }else{
                paused = false;
                notify();
            }
        }
    }

    void tempRead(){
        this.temp = Math.random() * 100;
        this.setChanged();
        this.notifyObservers();
    }
    public double getTemperature(){
        return temp;
    }

    public boolean isPaused() {
        return paused;
    }
}
