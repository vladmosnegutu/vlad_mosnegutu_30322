package Lab2;
import java.util.Scanner;

public class Exercise6 {

    public static int nonRecursive(int n){
        int factorial = 1;
        for(int i =1; i<=n;i++)
            factorial = factorial * i;
        return factorial;
    }
    public static int recursive(int x){
        if(x>0)
         return x*recursive(x-1);
        else
            return 1;
    }
    public static void main(String[] args) {
        int factorialN=1;
        int factorialR=1;

        Scanner console = new Scanner(System.in);
        int N = console.nextInt();
        factorialN = nonRecursive(N);
        factorialR = recursive(N);
        System.out.print("Non recursive: "+factorialN);
        System.out.print("\nrecursive: "+factorialR);
    }
}
