package Lab2;
import java.util.Scanner;

public class Exercise2 {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);
        int n;
        System.out.print("n = ");
        n = console.nextInt();
        if (n == 1)
            System.out.print("ONE");
        else if (n == 2)
            System.out.print("TWO");
        else if (n == 3)
            System.out.print("THREE");
        else if (n == 4)
            System.out.print("FOUR");
        else if (n == 5)
            System.out.print("FIVE");
        else if (n == 6)
            System.out.print("SIX");
        else if (n == 7)
            System.out.print("SEVEN");
        else if (n == 8)
            System.out.print("EIGHT");
        else if (n == 9)
            System.out.print("NINE");
        else
            System.out.print("OTHER");

        int b;
        System.out.print("\nb = ");
        b = console.nextInt();
        switch(b) {
            case 1: System.out.print("ONE"); break;
            case 2: System.out.print("TWO"); break;
            case 3: System.out.print("THREE"); break;
            case 4: System.out.print("FOUR"); break;
            case 5: System.out.print("FIVE"); break;
            case 6: System.out.print("SIX"); break;
            case 7: System.out.print("SEVEN"); break;
            case 8: System.out.print("EIGHT"); break;
            case 9: System.out.print("NINE"); break;
            default: System.out.print("OTHER"); break;
        }
    }
}