package Lab2;
import java.util.Scanner;

import java.util.Random;

public class Exercise7 {
    public static void main(String[] args) {
        Random gen = new Random();
        Scanner console = new Scanner(System.in);
        int num = gen.nextInt(20);
        int i =0;
        int guess;
        System.out.print("guess a number between 0 and 20\n");
        for (i = 0; i < 3; i++) {
            guess = console.nextInt();
            if(guess == num){
                System.out.print("You guessed it!");
                break;
            }
            else if(guess < num)
                System.out.print("Wrong answer, your number is too low\n");
            else if(guess > num)
                System.out.print("Wrong answer, your number is too high\n");
        }
        if(i==3)

            System.out.print("You lost");
    }
}
