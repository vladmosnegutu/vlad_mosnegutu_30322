package Lab2;

import java.util.Scanner;

public class Exercise4 {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);
        int n = console.nextInt();
        int[] v = new int[n];
        for (int i = 0; i < n; i++) {
            v[i] = console.nextInt();
        }
        int i, max = 0;

        for(i=0;i<n;i++){
            if(max<v[i])
                max = v[i];
        }
        System.out.print(max);
    }
}
