package Lab2;
import java.util.Scanner;

import java.util.Random;

public class Exercise5 {
        static void bubbleSort(int[] arr) {
            int n = arr.length;
            int temp = 0;
            for (int i = 0; i < n; i++)
                for (int j = 1; j < (n - i); j++)
                    if (arr[j - 1] > arr[j]) {
                        //swap elements
                        temp = arr[j - 1];
                        arr[j - 1] = arr[j];
                        arr[j] = temp;
                    }
        }


    public static void main(String[] args) {
        Random gen = new Random();
        int[] v = new int[10];
        for(int i =0; i<10; i++)
            v[i] = gen.nextInt(100);

        bubbleSort(v);

        for (int i = 0; i < 10; i++) {
            System.out.print(v[i] + " ");
        }
    }
}
