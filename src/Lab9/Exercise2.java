package Lab9;
//package isp.grafic.fereastra;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
import java.util.*;

public class Exercise2 extends JFrame{

    JTextArea tArea;
    JButton bUton;

    Exercise2(){
        setTitle("Test login");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(200,300);
        setVisible(true);
    }

    public void init() {

        this.setLayout(null);
        int width = 80;
        int height = 20;
        bUton = new JButton("buton");
        bUton.setBounds(10,10,width, height);
        bUton.addActionListener(new TratareButon());

        tArea = new JTextArea();
        tArea.setBounds(10,40,150,180);

        add(tArea); add(bUton);
    }
    public static void main(String[] args){
        new Exercise2();
    }
    class TratareButon implements ActionListener{
        Integer i = 0;
        public void actionPerformed(ActionEvent e) {
            i++;
            Exercise2.this.tArea.setText(null);
            Exercise2.this.tArea.append(i.toString() + '\n');
        }
    }
}


